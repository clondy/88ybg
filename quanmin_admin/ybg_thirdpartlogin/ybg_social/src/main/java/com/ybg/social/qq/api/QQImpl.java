/**
 * 
 */
package com.ybg.social.qq.api;
import java.util.Map;
import org.apache.commons.lang.StringUtils;
import org.springframework.social.oauth2.AbstractOAuth2ApiBinding;
import org.springframework.social.oauth2.TokenStrategy;
import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ybg.social.baidu.api.BaiduUserInfo;

/** @author zhailiang */
public class QQImpl extends AbstractOAuth2ApiBinding implements QQ {
	
	private static final String	URL_GET_OPENID		= "https://graph.qq.com/oauth2.0/me?access_token=%s";
	private static final String	URL_GET_USERINFO	= "https://graph.qq.com/user/get_user_info?oauth_consumer_key=%s&openid=%s";
	private String				appId;
	private String				openId;
	
	public QQImpl(String accessToken, String appId) {
		super(accessToken, TokenStrategy.ACCESS_TOKEN_PARAMETER);
		this.appId = appId;
		String url = String.format(URL_GET_OPENID, accessToken);
		String result = getRestTemplate().getForObject(url, String.class);
		this.openId = StringUtils.substringBetween(result, "\"openid\":\"", "\"}");
		
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.imooc.security.core.social.qq.api.QQ#getUserInfo()
	 */
	@Override
	public QQUserInfo getUserInfo() {
		String url = String.format(URL_GET_USERINFO, appId, openId);
		String result = getRestTemplate().getForObject(url, String.class);
		System.out.println(result+".39");
		QQUserInfo userInfo = null;
		try {
			
			userInfo = JSONObject.parseObject(result, QQUserInfo.class);
			userInfo.setOpenId(openId);
			return userInfo;
		} catch (Exception e) {
			throw new RuntimeException("获取用户信息失败", e);
		}
	}
}
