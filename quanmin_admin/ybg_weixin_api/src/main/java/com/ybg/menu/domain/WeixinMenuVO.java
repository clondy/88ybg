package com.ybg.menu.domain;
public class WeixinMenuVO {
	
	WeixinButton[] button;
	
	public WeixinButton[] getButton() {
		return button;
	}
	
	public void setButton(WeixinButton[] button) {
		this.button = button;
	}
}
